package com.example.cubanpete.kitenkat;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.example.cubanpete.kitenkat.model.Poll;
import com.example.cubanpete.kitenkat.model.PollOption;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class PollResultActivity extends AppCompatActivity {

    Button btnMenu;
    LinearLayout llPollResult;
    TextView tvPollTitle;
    FirebaseDatabase firebaseDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poll_result);
        firebaseDb = FirebaseDatabase.getInstance();

        llPollResult = findViewById(R.id.llPollResult);

        tvPollTitle = findViewById(R.id.tvPollTitle);

        btnMenu = findViewById(R.id.btnMenu);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        getPoll(getPollIdFromBundle());

    }

    private String getPollIdFromBundle(){
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            return  bundle.getString("POLLID");
        }
        return null;
    }

    private void getPoll(String pollId) {
        firebaseDb.getReference().child("polls").orderByChild("id").equalTo(pollId)
                .addValueEventListener( new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    Util.toasted(PollResultActivity.this, "Enquete não encontrada.");
                    finish();
                }
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Poll poll = ds.getValue(Poll.class);
                    llPollResult.removeAllViews();
                    setTitle(poll.getId());
                    tvPollTitle.setText(poll.getPollQuestion());
                    createOptionViews(poll);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void createOptionViews(Poll poll){
        for (PollOption option : poll.getPollOptions()){
            LayoutParams MPWCParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            LinearLayout optionLayout = new LinearLayout(this);
            optionLayout.setOrientation(LinearLayout.VERTICAL);
            MPWCParams.setMargins(0, 0, 0, 13);
            optionLayout.setLayoutParams(MPWCParams);
            optionLayout.setBackgroundColor(getResources().getColor(R.color.gray0));

            TextView optionTitleTextView = new TextView(this);
            optionTitleTextView.setText(option.getOptionTitle());
            optionTitleTextView.setPadding(30,30,30,10);
            optionTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19f);
            optionLayout.addView(optionTitleTextView);

            TextView optionVotesTextView = new TextView(this);
            optionVotesTextView.setText(String.format("Votos: %s", Integer.toString(option.getOptionVoteCount())));
            optionVotesTextView.setPadding(30,0,30,30);
            optionVotesTextView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            optionVotesTextView.setTypeface(null, Typeface.BOLD);
            optionLayout.addView(optionVotesTextView);

            llPollResult.addView(optionLayout);
        }
    }
}
