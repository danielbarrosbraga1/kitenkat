package com.example.cubanpete.kitenkat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PollSearchActivity extends AppCompatActivity {

    Button btnBack, btnPollResult, btnTakePoll;
    EditText EtPollId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poll_search);

        EtPollId = findViewById(R.id.EtPollId);
        btnBack = findViewById(R.id.btnBack);
        btnPollResult = findViewById(R.id.btnPollResult);
        btnTakePoll = findViewById(R.id.takePoll);

        String pollId = pollId();
        if (pollId != null) {
            EtPollId.setText(pollId);
        }
        btnTakePoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pollId = EtPollId.getText().toString();
                showPoll(pollId, PollActivity.class);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnPollResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pollId = EtPollId.getText().toString();
                showPoll(pollId, PollResultActivity.class);
            }
        });
    }

    private void showPoll(String pollId, Class activityClass) {
        if(pollId == null || "".equals(pollId)) {
            Util.toasted(this,"Insira o código da enquete.");
            return;
        }

        Intent intent = new Intent(this, activityClass);
        intent.putExtra("POLLID", pollId);
        finish();
        startActivity(intent);
    }

    private String pollId(){
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            return bundle.getString("POLLID");

        }
        return null;
    }
}
