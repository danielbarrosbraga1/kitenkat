package com.example.cubanpete.kitenkat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.cubanpete.kitenkat.model.Poll;
import com.example.cubanpete.kitenkat.model.PollOption;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

public class PollActivity extends AppCompatActivity {

    TextView tvPollTitle;
    RadioGroup rgPollOptions;
    Button btnMenu, btnVote;
    FirebaseDatabase firebaseDb;
    String pollId;
    String pollKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poll);
        firebaseDb = FirebaseDatabase.getInstance();

        tvPollTitle = findViewById(R.id.tvPollTitle);
        rgPollOptions = findViewById(R.id.RgPollOptions);
        btnMenu = findViewById(R.id.btnMenu);
        btnVote = findViewById(R.id.btnVote);

        pollId = getPollIdFromBundle();

        if (pollId == null) {
            Util.toasted(this,"Busca de Enquete falhou, tente mais tarde");
        } else {
            getPoll(getPollIdFromBundle());
        }

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendVote();
                showPoll(pollId);
            }
        });


    }

    private String getPollIdFromBundle(){
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            return  bundle.getString("POLLID");
        }
        return null;
    }

    private void getPoll(String pollId) {
        firebaseDb.getReference().child("polls").orderByChild("id").equalTo(pollId)
            .addListenerForSingleValueEvent( new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() == null) {
                        Util.toasted(PollActivity.this, "Enquete não encontrada.");
                        finish();
                    }
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        pollKey = ds.getKey();
                        Poll poll = ds.getValue(Poll.class);
                        setTitle(poll.getId());
                        tvPollTitle.setText(poll.getPollQuestion());
                        createOptionViews(poll);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
    }

    private void createOptionViews(Poll poll){
        int pollId = 0;
        for (PollOption option : poll.getPollOptions()){
            LayoutParams MPWCParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            MPWCParams.setMargins(0, 0, 0, 13);

            RadioButton rbOption = new RadioButton(this);
            rbOption.setText(option.getOptionTitle());
            rbOption.setPadding(30,30,30,30);
            rbOption.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
            rbOption.setLayoutParams(MPWCParams);
            rbOption.setId(pollId);
            rbOption.setBackgroundColor(getResources().getColor(R.color.gray0));

            rgPollOptions.addView(rbOption);
            pollId++;
        }
    }

    private void sendVote(){
        int selectedPollId = rgPollOptions.getCheckedRadioButtonId();
        DatabaseReference pollRf = firebaseDb.getReference().child("polls/" + pollKey);
        DatabaseReference selectedPollOptionRf = pollRf.child("pollOptions").child(Integer.toString(selectedPollId)).child("optionVoteCount").getRef();

        selectedPollOptionRf.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Integer voteCount = mutableData.getValue(Integer.class);
                if (voteCount == null) {
                    return Transaction.success(mutableData);
                }

                mutableData.setValue(voteCount + 1);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {}
        });
    }

    private void showPoll(String pollId) {
        Intent intent = new Intent(this, PollResultActivity.class);
        intent.putExtra("POLLID", pollId);
        finish();
        startActivity(intent);
    }

}
