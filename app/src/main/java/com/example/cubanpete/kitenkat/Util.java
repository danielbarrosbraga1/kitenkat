package com.example.cubanpete.kitenkat;

import android.content.Context;
import android.util.Base64;
import android.widget.Toast;

import java.security.SecureRandom;

public class Util {

    public static String getBase4RadomId() {
        SecureRandom rand = new SecureRandom();
        int encodeRule = Base64.NO_PADDING | Base64.URL_SAFE | Base64.NO_CLOSE | Base64.NO_WRAP;
        byte[] randomBytes = new byte[4];
        rand.nextBytes(randomBytes);
        return Base64.encodeToString(randomBytes, encodeRule).toLowerCase();
    }

    public static void toasted(Context context, String errorMsg){
        Toast toast = Toast.makeText(context, errorMsg, Toast.LENGTH_LONG);
        toast.show();
    }
}
