package com.example.cubanpete.kitenkat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.content.Context;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.example.cubanpete.kitenkat.model.Poll;
import com.example.cubanpete.kitenkat.model.PollOption;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class CreatePollActivity extends AppCompatActivity {

    Button btnCreatePool, btnBack, btnMoreOption;
    EditText etPollQuestion, etPollOption1, etPollOption2;

    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference pollsRf = firebaseDatabase.getReference("polls");

    private LinearLayout llPollForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_poll);

        llPollForm = findViewById(R.id.llPollForm);
        etPollQuestion = findViewById(R.id.etPollQuestion);
        etPollOption1 = findViewById(R.id.etPollOption1);
        etPollOption2 = findViewById(R.id.etPollOption2);
        btnCreatePool = findViewById(R.id.btnCreatePoll);
        btnMoreOption = findViewById(R.id.btnMoreOption);
        btnBack = findViewById(R.id.btnBack);

        btnMoreOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rowView = inflater.inflate(R.layout.poll_form_option_component, null);
                llPollForm.addView(rowView, llPollForm.getChildCount());
            }
        });

        btnCreatePool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createPoll();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public void createPoll() {

        String pollQuestion = etPollQuestion.getText().toString();
        String pollOption1 = etPollOption1.getText().toString();
        String pollOption2 = etPollOption2.getText().toString();
        ArrayList<PollOption> pollOptions = new ArrayList<>();
        pollOptions.add( new PollOption(pollOption1, 0));
        pollOptions.add( new PollOption(pollOption2, 0));
        ArrayList<String> adicionalOptionsTitles = null;

        if(pollQuestion.matches("") || pollOption1.matches("") || pollOption2.matches("")) {
            Toast.makeText(getApplicationContext(), "Campos não preenchidos", Toast.LENGTH_LONG).show();
            return;
        }

        if (llPollForm.getChildCount()>3) {
            adicionalOptionsTitles = getOptionsFromListView();
        }

        boolean emptyAdicionalOption = false;
        if(adicionalOptionsTitles != null) {
            for (String titles :getOptionsFromListView()) {
                if (titles.matches("")) {
                    emptyAdicionalOption = true;
                }
                pollOptions.add(new PollOption(titles, 0));
            }
        }

        if (emptyAdicionalOption) {
            return;
        }

        String pollId = Util.getBase4RadomId();
        Poll poll = new Poll(pollId, pollQuestion, pollOptions);

        try {
            pollsRf.push().setValue(poll);
        } catch (Exception e){
            Util.toasted(CreatePollActivity.this, "Não foi possível efetuar a gravação nos nossos servidores, tente mais tarde.");
        }

        Intent intent = new Intent(this, PollSearchActivity.class);
        intent.putExtra("POLLID", pollId);
        finish();
        startActivity(intent);
    }

    private ArrayList<String> getOptionsFromListView() {
        llPollForm.getChildCount();
        ArrayList<String> optionsTitles = new ArrayList<>();
        for( int i = 3; i != llPollForm.getChildCount(); i++) {
            LinearLayout linearLayout = (LinearLayout) llPollForm.getChildAt(i);
            EditText editText = (EditText) linearLayout.getChildAt(0);
            optionsTitles.add(editText.getText().toString());
        }

        return optionsTitles;

    }

    public void onDelete(View v) {
        llPollForm.removeView((View) v.getParent());
    }

}
