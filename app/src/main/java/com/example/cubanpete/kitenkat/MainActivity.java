package com.example.cubanpete.kitenkat;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btnCreatePoll, btnPoll;
    TextView tvLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Typeface kitkatFont = Typeface.createFromAsset(this.getAssets(), "fonts/KittyKatt.ttf");

        btnCreatePoll = findViewById(R.id.btnCreatePoll);
        btnPoll = findViewById(R.id.btnPoll);
        tvLogo = findViewById(R.id.tvLogo);
        tvLogo.setTypeface(kitkatFont);
        tvLogo.setText(R.string.app_name);

        btnCreatePoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CreatePollActivity.class));
            }
        });

        btnPoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, PollSearchActivity.class));
            }
        });
    }
}
