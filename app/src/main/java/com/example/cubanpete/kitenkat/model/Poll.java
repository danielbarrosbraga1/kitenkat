package com.example.cubanpete.kitenkat.model;

import java.util.ArrayList;

public class Poll {

    String id;
    String pollQuestion;
    ArrayList<PollOption> pollOptions;

    public Poll(String id, String pollQuestion, ArrayList<PollOption> pollOptions) {
        this.id = id;
        this.pollQuestion = pollQuestion;
        this.pollOptions = pollOptions;
    }

    public Poll(){}

    public String getPollQuestion() {
        return pollQuestion;
    }

    public void setPollQuestion(String pollQuestion) {
        this.pollQuestion = pollQuestion;
    }

    public ArrayList<PollOption> getPollOptions() {
        return pollOptions;
    }

    public void setPollOptions(ArrayList<PollOption> pollOptions) {
        this.pollOptions = pollOptions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
