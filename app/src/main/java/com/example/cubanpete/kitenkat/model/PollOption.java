package com.example.cubanpete.kitenkat.model;

public class PollOption {

    String optionTitle;
    int optionVoteCount;

    public PollOption(){}
    public PollOption(String optionTitle, int optionVoteCount) {
        this.optionTitle = optionTitle;
        this.optionVoteCount = optionVoteCount;
    }

    public String getOptionTitle() {
        return optionTitle;
    }

    public void setOptionTitle(String optionTitle) {
        this.optionTitle = optionTitle;
    }

    public int getOptionVoteCount() {
        return optionVoteCount;
    }

    public void setOptionVoteCount(int optionVoteCount) {
        this.optionVoteCount = optionVoteCount;
    }
}
